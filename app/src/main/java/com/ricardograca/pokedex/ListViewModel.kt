package com.ricardograca.pokedex

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.ricardograca.pokedex.pojo.Pokemon
import com.ricardograca.pokedex.repository.IPokemonRepository
import com.ricardograca.pokedex.repository.PokemonRepository

class ListViewModel : ViewModel(), IPokemonRepository.ICallback {
    val list = arrayListOf<Pokemon>()
    lateinit var data : MutableLiveData<List<Pokemon>>
    val repository : IPokemonRepository = PokemonRepository()

    var offset = 0
    var loading = false

    fun getPokemons() : LiveData<List<Pokemon>> {
        loading = true
        repository.getPokemons(callback = this)

        data = MutableLiveData()

        return data
    }

    fun loadMorePokemons() {
        if (!loading) {
            loading = true
            offset += 20

            repository.getPokemons(offset = offset, callback = this)
        }
    }

    override fun onPokemonsLoaded(pokemonList: List<Pokemon>) {
        list.addAll(pokemonList)

        data.value = list
        loading = false
    }
}