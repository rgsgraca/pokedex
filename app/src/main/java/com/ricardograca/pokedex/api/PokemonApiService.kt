package com.ricardograca.pokedex.api

import com.ricardograca.pokedex.pojo.Pokemon
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface PokemonApiService {
    @GET("pokemon/{number}")
    fun getPokemon(@Path("number") name : Int) : Call<Pokemon>
}