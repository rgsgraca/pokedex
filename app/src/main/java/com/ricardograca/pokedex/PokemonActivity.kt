package com.ricardograca.pokedex

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.ricardograca.pokedex.pojo.Pokemon
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_pokemon.*

class PokemonActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_pokemon)

        var pokemon = intent.getParcelableExtra<Pokemon>("pokemon")

        Picasso.get().load(pokemon.sprites.front_default).into(image)
    }
}