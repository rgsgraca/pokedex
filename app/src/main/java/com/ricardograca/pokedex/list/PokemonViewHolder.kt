package com.ricardograca.pokedex.list

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.ricardograca.pokedex.R

class PokemonViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
    val nameView : TextView
    val imageView : ImageView
    init {
        nameView = view.findViewById(R.id.name)
        imageView = view.findViewById(R.id.image)
    }
}