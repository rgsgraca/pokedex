package com.ricardograca.pokedex.list

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.ricardograca.pokedex.R
import com.ricardograca.pokedex.pojo.Pokemon
import com.squareup.picasso.Picasso

class PokemonListAdapter(val clickListener : IClickListener) : RecyclerView.Adapter<PokemonViewHolder>() {
    interface IClickListener {
        fun onPokemonClicked(pokemon: Pokemon, imageView : ImageView)
    }

    private var pokemonList = arrayListOf<Pokemon>()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PokemonViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_row, parent, false)

        return PokemonViewHolder(view)
    }

    override fun getItemCount() = pokemonList.size

    override fun onBindViewHolder(holder: PokemonViewHolder, position: Int) {
        val pokemon = pokemonList[position]

        holder.nameView.text = pokemon.name
        Picasso.get().load(pokemon.sprites.front_default).into(holder.imageView)
        holder.view.setOnClickListener {
            clickListener.onPokemonClicked(pokemon, holder.imageView)
        }
    }

    fun setPokemons(list : List<Pokemon>) {
        val sortedList = list.sortedWith(compareBy { it.order })
        pokemonList.clear()
        pokemonList.addAll(sortedList)
        notifyDataSetChanged()
    }
}