package com.ricardograca.pokedex

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import androidx.core.app.ActivityOptionsCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ricardograca.pokedex.list.PokemonListAdapter
import com.ricardograca.pokedex.pojo.Pokemon
import kotlinx.android.synthetic.main.activity_main.*
import timber.log.Timber

class MainActivity : AppCompatActivity(), PokemonListAdapter.IClickListener {
    private lateinit var viewModel : ListViewModel
    private lateinit var viewAdapter: PokemonListAdapter
    private lateinit var viewManager: LinearLayoutManager


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        viewModel = ViewModelProviders.of(this).get(ListViewModel::class.java)

        viewManager = LinearLayoutManager(this)
        viewAdapter = PokemonListAdapter(this)

        recyclerView.apply {
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = viewAdapter

            addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    super.onScrolled(recyclerView, dx, dy)

                    var totalItemCount = viewManager?.itemCount

                    var lastVisibleItem = viewManager?.findLastCompletelyVisibleItemPosition()
                    if (totalItemCount <= (lastVisibleItem + 5)) {
                        // End has been reached
                        // Do something
                        viewModel.loadMorePokemons()
                    }
                }
            });
        }

        viewModel.getPokemons().observe(this, Observer {
            if (it != null) {
                viewAdapter.setPokemons(it)
            }
        } )
    }

    override fun onPokemonClicked(pokemon: Pokemon, imageView : ImageView) {
        var intent = Intent(this, PokemonActivity::class.java)
        intent.putExtra("pokemon", pokemon)
        var options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, imageView, "pokemonImage")
        startActivity(intent, options.toBundle())
    }
}
