package com.ricardograca.pokedex.repository

import androidx.lifecycle.LiveData
import com.ricardograca.pokedex.pojo.Pokemon
import com.ricardograca.pokedex.pojo.PokemonItemList

interface IPokemonRepository {
    interface ICallback {
        fun onPokemonsLoaded(pokemonList : List<Pokemon>)
    }

    fun getPokemons(offset : Int = 0 , limit : Int = 20, callback: ICallback)
}