package com.ricardograca.pokedex.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.ricardograca.pokedex.api.PokemonApiService
import com.ricardograca.pokedex.pojo.Pokemon
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import timber.log.Timber

class PokemonRepository : IPokemonRepository{
    private val service : PokemonApiService
    init {
        val retrofit = Retrofit.Builder()
                            .baseUrl("https://pokeapi.co/api/v2/")
                            .addConverterFactory(GsonConverterFactory.create())
                            .build()

        service = retrofit.create(PokemonApiService::class.java)
    }

    override fun getPokemons(offset : Int, limit : Int, callback: IPokemonRepository.ICallback) {
        Timber.d("getPokemons")
        val list = arrayListOf<Pokemon>()

        for (i in offset+1 until offset+limit) {
            service.getPokemon(i).enqueue(object : Callback<Pokemon> {
                override fun onFailure(call: Call<Pokemon>, t: Throwable) {
                    callback.onPokemonsLoaded(list)
                }

                override fun onResponse(call: Call<Pokemon>, response: Response<Pokemon>) {
                    if (response.isSuccessful) {
                        Timber.d("Success $i")
                        response.body()?.let {
                            list.add(it)
                            if (list.size == limit-1) {
                                callback.onPokemonsLoaded(list)
                            }
                        }
                    }
                }
            })
        }
    }

}